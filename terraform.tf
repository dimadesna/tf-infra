terraform {
  required_version = "~> 0.12.0"


  backend "s3" {
    bucket         = "tf-state-files-db"
    region         = "eu-central-1"
    key            = "state/eu-central-1/dev/app.tfstate"
 #   dynamodb_table = "terraform-lock"
  }
}
