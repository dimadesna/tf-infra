variable "app_instance_type" {}
variable "key_name" {}

variable "app_ami" {
  default = "ami-0520e698dd500b1d1"
}
variable "region" {
  default = "eu-central-1"
}

variable "ec2_name" {
  default = { Name = "app-dev" }
}
variable "environment" {
}

variable "vpc_id" {
}

variable "private_subnet_id" {
}

variable "vpc_cidr" {
}

variable "tags" {
}