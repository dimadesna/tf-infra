module "app" {
# source                = "git::ssh://git@"
  source                = "app.terraform.io/dmytro-bilan/module/infra"
#  source  = "../tf-infra-module"
  vpc_id                = var.vpc_id
  private_subnet_id     = var.private_subnet_id
  vpc_cidr              = var.vpc_cidr

  #vpc_id                = data.terraform_remote_state.vpc.outputs.vpc_id
  #private_subnet_id     = data.terraform_remote_state.vpc.outputs.private_subnets
  #vpc_cidr              = data.terraform_remote_state.vpc.outputs.vpc_cidr
  ami                   = var.app_ami
  instance_type         = var.app_instance_type
  key_name              = var.key_name
  #domain_name           = var.sub_domain
  #route_zone_id         = var.route_zone_id
  #certificate_arn       = var.certificate_arn
  ec2_name              = var.ec2_name
  tags                  = var.tags
  environment           = var.environment
  #instance_profile_name = var.instance_profile_name
}

  