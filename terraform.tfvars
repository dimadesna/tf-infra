app_instance_type = "t2.micro"
key_name              = "dmytrobilan"
#certificate_arn       = "..."
tags = {
  Environment     = "DEV"
  Project         = "Project"
  Purpose         = "Preview"
  ResponsibleTeam = "DevOPS"
}
environment = "sbx"
vpc_id                = "vpc-16c1397d"
private_subnet_id     = ["172.31.0.0/20"]
vpc_cidr              = "172.31.0.0/16"
